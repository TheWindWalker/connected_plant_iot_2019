/*
 * Project SuperSpinnach3K
 * Description: IoT Plant connected
 * Author: Anthoine Cherel, Arnaud Borio, Baptiste, Fabien, Matthieu Le Biavant
 * Date: 10/12/2019
 */

//==============================
//       Dependencies
//==============================
//Lib for Light sensor
#include <Adafruit_TSL2591.h>

//Lib: SparkFunRHT03 :  RHT03 temperature and humidity sensor
#include <SparkFunRHT03.h>

//#include "screenclass.h"
/*
For using the screen (screen epaper Waveshare)
uncomment this include and Screen initialization then all the display-> call
 */

//Threading for async
#include "Particle.h"
SYSTEM_THREAD(ENABLED);
SYSTEM_MODE(AUTOMATIC);
//==============================
//       Global variables
//==============================
#define DEBUGGING_CONCEP true

//Pinout Assignement
const int RHT_Sensor_DATA_PIN  = D6; // RHT03 data pin
const int SoilMoist_Sensor_DATA_PIN = A0; //Analog input
const int SoilMoist_Sensor_PowerPin = A4;
const int PresenceSensorPin = A1;
const int PWM_BUZZER_PIN = D3;
const int PWM_Servo_Gauge = TX; //Gauge humidity air
const int PlantSensor_Pin = D4; //D4
/*for screen: (In epdif.h)
const unsigned int CS_PIN = A2;
const unsigned int DC_PIN = D2; //D4
const unsigned int RST_PIN = RX; //D6
const unsigned int BUSY_PIN = D5; //D5
//SPI : A3 / A5
*/
//D7 is used for visual recognition of touch of the plant


//Hygrometre global values
RHT03 RHT_Sensor; // This creates a RTH03 object, which we'll use to interact with the sensor
volatile float latestHumidityAir = 0.0f;
volatile float latestTempC = 0.0f;
unsigned long last_Hygro_capture = 0;

//Soil Moisture sensor
float latestSoilHumidity = 0.0f;

//Light Sensor
Adafruit_TSL2591 LightSensor = Adafruit_TSL2591(2591); // pass in a number for the sensor identifier (for your use later)
volatile uint16_t latestLux = 0.0f;
volatile uint16_t latestIR = 0.0f;
volatile uint16_t latestVIS = 0.0f;
volatile uint16_t latestFULL = 0.0f;

//Presence sensor
volatile bool SomeOneHasBeenSeenRecently = false;
volatile unsigned long LastSeenEvent = 0;
volatile unsigned long LastSeenTimeOut = 30000; //30s time to disable the new event (Warning, communication take time!)

//Plant Contact:
volatile bool isPlantContact = false;
volatile bool isPlantContactPublish = false;
volatile unsigned int PlantContactCount = 0;
volatile bool PlantContactEvent = false;
volatile unsigned long PlantContactTimer = 0;
volatile unsigned long MaxTimerPlantContact = 60000; //1Min
volatile uint16_t cycle_thres = 10; //threshold for plant detection 10

//Screen
//Screen *display;

//Each 10ms the thread will look for the press of the plant (like a button)
void thread_PlantContact_Input(void *param);
Thread threadPlantContact("PlantContactInterrupt", thread_PlantContact_Input);

Thread threadTransmit_Data("Transmit_Data", Transmit_Data);

volatile unsigned long TimeAtLastUpdate = 0; //ms
#ifdef DEBUGGING_CONCEP
volatile unsigned long TimeWaitUpdate = 20000; //ms Transmit each 10 seconds
#else
volatile unsigned long TimeWaitUpdate = 600000; //ms Transmit each 10 minutes
#endif

volatile bool IsglobalSensorsSetup = false;
//==============================
//       Global Functions
//==============================
void setup() { 
  Serial.begin(9600);
  pinMode(D7, OUTPUT);
  HygroMetre_Setup();
  SoilMoisture_Setup();
  Light_Sensor_Setup();
  PresenceSensor_Setup();
  PlantContact_Setup();
  PWM_Servo_Gauge_Setup();
  //display = new Screen (HAPPY, CLOUDY);
  IsglobalSensorsSetup = true;
}

void loop() {
  Update();
}

void Update()
{
  HygroMetre_Update();
  SoilMoisture_Update();
  Light_Sensor_Update(); //100 - 600 MS delay for capturing data
  PresenceSensor_Update();
  PlantContact_Update();
  PWM_Servo_Gauge_Update();
}

void Transmit_Data() //For serial communication
{
  while(!IsglobalSensorsSetup){os_thread_yield();}
  while(true)
  {
    //Transmit if timer is ready
    unsigned long CurrentTime = millis();//49 Days before overflow
    if( (CurrentTime - TimeAtLastUpdate) >= TimeWaitUpdate )
    {
      TimeAtLastUpdate = CurrentTime;

      //Send Hygrometer Values
      /*Serial.println("AirMoist: " + String(latestHumidityAir, 1) + " %");
      Serial.println("AirTempC: " + String(latestTempC, 1) + " deg C");
      //Send Soil moisture value
      Serial.println("SoilMoist: " + String(latestSoilHumidity, 1) + " %");
      Serial.println("LuxLevel: " + String(latestLux) + "lux");
      Serial.println("IRLevel" + String(latestIR));
      Serial.println("VISLevel: " + String(latestVIS));
      Serial.println("FULLLevel: " + String(latestFULL));
      Serial.println("Presence: " + String(SomeOneHasBeenSeenRecently));
      Serial.println("PlantContactCount: " + String(PlantContactCount));*/
      //temperature will take the whole trame of data , separated by ";"

      Particle.publish("temperature",String(latestHumidityAir) + ";" +
       String(latestTempC) + ";"+ 
       String(latestSoilHumidity) + ";"+
       String(latestLux)  + ";"+
       String(isPlantContactPublish)
        );
      delay(1000);
      /*
      Particle.publish("air_humidity",String(latestHumidityAir));
      delay(1000); //Delay between each transmit
      Particle.publish("temperature",String(latestTempC));
      delay(1000); //Delay between each transmit
      Particle.publish("soil_humidity",String(latestSoilHumidity));
      delay(1000); //Delay between each transmit
      Particle.publish("brightness",String(latestLux));
      delay(1000); //Delay between each transmit*/

      /*Particle.publish("IRLevel",String(latestIR));
      delay(1000); //Delay between each transmit
      Particle.publish("VISLevel",String(latestVIS));
      delay(1000); //Delay between each transmit
      Particle.publish("FULLLevel",String(latestFULL));
      delay(1000); //Delay between each transmit
      Particle.publish("Presence",String(SomeOneHasBeenSeenRecently));
      delay(1000); //Delay between each transmit*/
      if(isPlantContactPublish)
      {
        isPlantContactPublish = false;
        //display->change_mood(HAPPY);
        //display->refresh();
        //Particle.publish("pet_plant", "1");
        //delay(1000);
      }
      else
      {
        //display->change_mood(SAD);
        //display->refresh();
      }
      
      //Particle.publish("validate", "validate");
      //delay(1000); //Delay between each transmit
    }
    if(CurrentTime < TimeAtLastUpdate) //OverFlowed Millis() timer
    {
      TimeAtLastUpdate = CurrentTime;
    }
    os_thread_yield();
  }
}

//==============================
//       Features
//==============================

void HygroMetre_Setup()
{
  /* Looking at the front (grated side) of the RHT03, the pinout is as follows:
	 1     2        3       4
	VCC  DATA  No-Connect  GND
  Connect the data pin to Photon pin D3. Power the RHT03 off the 3.3V bus. */
  SINGLE_THREADED_BLOCK() {
    RHT_Sensor.begin(RHT_Sensor_DATA_PIN); //initialize the sensor and our data pin
    delay(1000);
  }
}

void HygroMetre_Update()
{
  //Permit only this operation (this sensor need monothreaded use)
  SINGLE_THREADED_BLOCK() {
    unsigned long currentTime = millis();
    //Minimal Interval for the Humidity sensor (1000ms)
    if((currentTime - last_Hygro_capture) >= RHT_READ_INTERVAL_MS)
    {
      last_Hygro_capture=currentTime;
      // Call rht.update() to get new values of humidity and temperature from the sensor.
      int updateRet = RHT_Sensor.update();
      if(updateRet == 0) return;
      //Get values
      latestHumidityAir = RHT_Sensor.humidity();
      latestTempC = RHT_Sensor.tempC();
    }
  }
}

void SoilMoisture_Setup()
{
  pinMode(SoilMoist_Sensor_PowerPin, OUTPUT);
  pinMode(SoilMoist_Sensor_DATA_PIN, AN_INPUT);
}

void SoilMoisture_Update()
{
  //This sensor may interfer with the plant touch sensor (capacitance of the plant) because of the grounding pin
  // Read the analog value of the sensor (SEN-13637)
  digitalWrite(SoilMoist_Sensor_PowerPin,HIGH);
  int32_t analogvalue = analogRead(SoilMoist_Sensor_DATA_PIN);
  digitalWrite(SoilMoist_Sensor_PowerPin,LOW);
  //Convert the reading
  //Resolution ADC: 4095, Voltage sensor: 3.3V;
  float percentage = (analogvalue * 100) / 4095;
  percentage = percentage + percentage/100 * 18;
  if(percentage <=0) percentage = 0.0f;
  latestSoilHumidity = percentage ;//+ percentage/100 * 19; //corrected value (calibrated)
}

void Light_Sensor_Setup()
{
  // TSL2591 Digital Light Sensor
  // Dynamic Range: 600M:1
  // Maximum Lux: 88K
  // connect SCL to I2C Clock
  // connect SDA to I2C Data
  // connect Vin to 3.3-5V DC

  //Light Sensor
  bool LightSensor_Init = false;
  float latestLux = 0.0f;
  float latestIR = 0.0f;
  float latestVIS = 0.0f;
  LightSensor.begin();
  // Configures the gain and integration time for the TSL2591
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations

  //LightSensor.setGain(TSL2591_GAIN_LOW);    // 1x gain (bright light)
  LightSensor.setGain(TSL2591_GAIN_MED);      // 25x gain
  //LightSensor.setGain(TSL2591_GAIN_HIGH);   // 428x gain
  
  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  //LightSensor.setTiming(TSL2591_INTEGRATIONTIME_100MS);  // shortest integration time (bright light)
  // LightSensor.setTiming(TSL2591_INTEGRATIONTIME_200MS);
  LightSensor.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  // LightSensor.setTiming(TSL2591_INTEGRATIONTIME_400MS);
  // LightSensor.setTiming(TSL2591_INTEGRATIONTIME_500MS);
  // LightSensor.setTiming(TSL2591_INTEGRATIONTIME_600MS);  // longest integration time (dim light)
}

void Light_Sensor_Update()
{
  // Show how to read IR and Full Spectrum at once and convert to lux
  // Read 32 bits with top 16 bits IR, bottom 16 bits full spectrum
  uint32_t lum = LightSensor.getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;
  latestLux = LightSensor.calculateLux(full, ir);
  latestIR = ir;
  latestVIS = full - ir;
  latestFULL = full;
}

void PresenceSensor_Setup()
{
  pinMode(PresenceSensorPin, INPUT_PULLDOWN);
  attachInterrupt(PresenceSensorPin, PresenceSensor_Interrupt, RISING);
}

void PresenceSensor_Update()
{
  //Detect if moving, if not and the timeout expire the flag will be reset to false
  unsigned long CurrentTime = millis();
  if (SomeOneHasBeenSeenRecently && ((CurrentTime - LastSeenEvent) >= LastSeenTimeOut ))
  {
    LastSeenEvent = 0;
    SomeOneHasBeenSeenRecently = false;
  }
  if(LastSeenEvent > CurrentTime) //Overflowed timer
  {
    LastSeenEvent = millis();
  }
}

void PresenceSensor_Interrupt()
{
  if(!SomeOneHasBeenSeenRecently)
  {
    SomeOneHasBeenSeenRecently = true;
    LastSeenEvent = millis();
  }
}

void PWM_Servo_Gauge_Setup()
{
  pinMode(PWM_Servo_Gauge,OUTPUT);
  analogWrite(PWM_Servo_Gauge, 12, 50);
  delay(1000);
}

void PWM_Servo_Gauge_Update()
{
  analogWrite(PWM_Servo_Gauge, map(latestHumidityAir, 0.0,100.0, 13.0,24.0), 50);
}

void PlantContact_Setup()
{
  pinMode(PWM_BUZZER_PIN, OUTPUT);
}

void PlantContact_Update () {
  unsigned long currentTime = millis();
  if((currentTime - PlantContactTimer) >= MaxTimerPlantContact)
  {
    PlantContactCount = 0;
  }

  if(isPlantContact)
  {
    tone(PWM_BUZZER_PIN, 450,1000);
    isPlantContact=false;
  }
}

void thread_PlantContact_Input(void * params)
{
  while(!IsglobalSensorsSetup){os_thread_yield();}
  system_tick_t lastThreadTime = 0;
  while(1)
  {
    //PlantContact is wired to VCC through a 1M Ohm resistor
    //Emptying capacitance
    digitalWrite (PlantSensor_Pin, LOW);
    pinMode (PlantSensor_Pin, OUTPUT);
    delayMicroseconds(1000);

    //mesure du nombre de cycles
    uint8_t cycles;
    pinMode (PlantSensor_Pin, INPUT);
    for(cycles = 0; cycles < 256; ++cycles){
      if (digitalRead (PlantSensor_Pin) == HIGH)
        // Si la broche a commuté on quitte la boucle
        break;
    }

    //Empty capacitance
    digitalWrite (PlantSensor_Pin, LOW);
    pinMode (PlantSensor_Pin, OUTPUT);
    //return true if cycle counter > threshold
    if (cycles > cycle_thres)
    {
      pinMode(D7,OUTPUT);
      digitalWrite(D7, HIGH);
      delay(50);
      if(!isPlantContact && !PlantContactEvent) //Only count if this is a new touch
      {
        PlantContactCount++;
      }
      isPlantContact = true;
      isPlantContactPublish = true;
      PlantContactEvent = true;
      pinMode(D7,OUTPUT);
      digitalWrite(D7, LOW);
      delay(50);
    }
    else
    {
      PlantContactEvent = false;
    }
    //Delay so we're called every 50 milliseconds (20 times per second)
    delay(10);
		os_thread_delay_until(&lastThreadTime, 50); //block the hygrometer!
    //os_thread_yield();
  }
}