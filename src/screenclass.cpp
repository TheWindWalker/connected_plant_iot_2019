#include "screenclass.h"


Screen::Screen (Mood mood, Meteo meteo) {
	if (epd.Init() != 0) {
		Serial.println ("e-Paper init failed");
		return;
	}
	epd.ClearFrame();

	currentMood = mood;
	currentMeteo = meteo;
	
	const int x = 128;
	const int y = 24;

	unsigned char img [x*y/8] = {0};
	Paint paint (img, x, y);
	paint.Clear (UNCOLORED);
	paint.DrawStringAt (0, 0, "Super", &Font24, COLORED);
	epd.SetPartialWindow (paint.GetImage (), 200, 200, x, y);
	paint.Clear (UNCOLORED);
	paint.DrawStringAt (0, 0, "Spinach", &Font24, COLORED);
	epd.SetPartialWindow (paint.GetImage (), 200, 230, x, y);
	paint.Clear (UNCOLORED);
	paint.DrawStringAt (0, 0, "3000", &Font24, COLORED);
	epd.SetPartialWindow (paint.GetImage (), 200, 260, x, y);

	refresh ();
	epd.Sleep ();
}


void Screen::change_mood (Mood newMood) {
	currentMood = newMood;
}

void Screen::change_meteo (Meteo newMeteo) {
	currentMeteo = newMeteo;
}

void Screen::refresh () {
	epd.WakeUp ();
	epd.SetPartialWindow (meteo_array [currentMeteo], 200, EPD_HEIGHT/2-meteoSizeY/2 - 50, meteoSizeX, meteoSizeY);
	epd.SetPartialWindow (mood_array [currentMood], 10, EPD_HEIGHT/2-moodSizeY/2, moodSizeX, moodSizeY);

	epd.DisplayFrame ();
	epd.Sleep ();
}

