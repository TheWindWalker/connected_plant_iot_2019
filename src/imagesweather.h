
#ifndef METEO
#define METEO

const int meteoSizeX = 96;
const int meteoSizeY = 96;

extern const unsigned char IMAGE_CLOUDY [];
extern const unsigned char IMAGE_SUN_CLOUDY[];
extern const unsigned char IMAGE_THUNDER[];
extern const unsigned char IMAGE_SNOW [];
extern const unsigned char IMAGE_NIGHT[];
extern const unsigned char IMAGE_SUNNY [];


extern const unsigned char *meteo_array [6];


#endif
