#ifndef SCREEN
#define SCREEN


#include "Particle.h"
#include "epd4in2.h"
#include "epdpaint.h"
#include "imagesmood.h"
#include "imagesweather.h" 

#define COLORED     0
#define UNCOLORED   1

enum Mood {
	HAPPY,
	BOF,
	SAD
};

enum Meteo {
	CLOUDS,
	CLOUDY,
	THUNDER,
	SNOW,
	NIGHT,
	SUNNY
};

class Screen {
	public :
		Screen (Mood mood, Meteo meteo);
		void change_mood (Mood mood);
		void change_meteo (Meteo meteo);
		void refresh ();

	protected :


	private :
		Mood currentMood;
		Meteo currentMeteo;
		Epd epd;
};

#endif
